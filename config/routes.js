const express = require("express");
const controllers = require("../app/controllers");

const appRouter = express.Router();
const apiRouter = express.Router();

/** Mount GET / handler */
appRouter.get("/", controllers.main.index);

/**
 * TODO: Implement Electronics API
 *       implementations
 */
apiRouter.get("/api/v1/electronics", controllers.api.v1.electronic.list);
apiRouter.post("/api/v1/electronics", controllers.api.v1.electronic.create);
apiRouter.put(
  "/api/v1/electronics/:id",
  controllers.api.v1.electronic.setElectronic,
  controllers.api.v1.electronic.update
);
apiRouter.get(
  "/api/v1/electronics/:id",
  controllers.api.v1.electronic.setElectronic,
  controllers.api.v1.electronic.show
);
apiRouter.delete(
  "/api/v1/electronics/:id",
  controllers.api.v1.electronic.setElectronic,
  controllers.api.v1.electronic.destroy
);

/**
 * TODO: Implement Warehouses API
 *       implementations
 */
 apiRouter.get("/api/v1/warehouses", controllers.api.v1.warehouse.list);
 apiRouter.post("/api/v1/warehouses", controllers.api.v1.warehouse.create);
 apiRouter.put(
   "/api/v1/warehouses/:id",
   controllers.api.v1.warehouse.setWarehouse,
   controllers.api.v1.warehouse.update
 );
 apiRouter.get(
   "/api/v1/warehouses/:id",
   controllers.api.v1.warehouse.setWarehouse,
   controllers.api.v1.warehouse.show
 );
 apiRouter.delete(
   "/api/v1/warehouses/:id",
   controllers.api.v1.warehouse.setWarehouse,
   controllers.api.v1.warehouse.destroy
 );


/**
 * TODO: Delete this, this is just a demonstration of
 *       error handler
 */
apiRouter.get("/api/v1/errors", () => {
  throw new Error(
    "The Industrial Revolution and its consequences have been a disaster for the human race."
  );
});

apiRouter.use(controllers.api.main.onLost);
apiRouter.use(controllers.api.main.onError);

/**
 * TODO: Delete this, this is just a demonstration of
 *       error handler
 */
appRouter.get("/errors", () => {
  throw new Error(
    "The Industrial Revolution and its consequences have been a disaster for the human race."
  );
});

appRouter.use(apiRouter);

/** Mount Not Found Handler */
appRouter.use(controllers.main.onLost);

/** Mount Exception Handler */
appRouter.use(controllers.main.onError);

module.exports = appRouter;

'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Electronics extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
        Electronics.belongsTo(models.Warehouse, {
          foreignKey: 'warehouseId'
        })
    }
  }
  Electronics.init({
    name: DataTypes.STRING,
    color: {
      type: DataTypes.STRING,
      defaultValue: 'black'
    },
    brand: DataTypes.STRING,
    price: DataTypes.INTEGER,
    warehouseId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Electronics',
  });
  return Electronics;
};
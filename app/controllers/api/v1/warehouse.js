/**
 * @file contains request handler of post resource
 * @author Safira Tyas Wandita
 */

 const { Warehouse } = require("../../../models/warehouse");
 const { Electronics } = require("../../../models/electronic")
 const warehouseService = require("../../../services/warehouse");
 
 module.exports = {
   list(req, res) {
     warehouseService
       .getAll({
         include: {
           model: Electronics
         },
       })
       .then((warehouse) => {
         res.status(200).json({
           status: "OK",
           data: {
             warehouse,
           },
         });
       })
       .catch((err) => {
         res.status(400).json({
           status: "FAIL",
           message: err.message,
         });
       });
   },
 
   create(req, res) {
     const { name, address, owner } = req.body;
     warehouseService
       .create(name, address, owner)
       .then((warehouse) => {
         res.status(201).json({
           status: "OK",
           data: warehouse,
         });
       })
       .catch((err) => {
         res.status(201).json({
           status: "FAIL",
           message: err.message,
         });
       });
   },
 
   update(req, res) {
     warehouseService
       .update(req.warehouse, req.body)
       .then((warehouse) => {
         res.status(200).json({
           status: "OK",
           data: warehouse,
         });
       })
       .catch((err) => {
         res.status(422).json({
           status: "FAIL",
           message: err.message,
         });
       });
   },
 
     show(req, res) {
       const warehouse = req.warehouse;
      
       res.status(200).json({
         status: "OK",
         data: warehouse,
       });
     },
   
     destroy(req, res) {
       warehouseService
         .delete(req.warehouse)
         .then(() => {
           res.status(204).end();
         })
         .catch((err) => {
           res.status(422).json({
             status: "FAIL",
             message: err.message,
           });
         });
     },
   
     setWarehouse(req, res, next) {
       warehouseService
         .choose(req.params.id)
         .then((warehouse) => {
           if (!warehouse) {
             res.status(404).json({
               status: "FAIL",
               message: "Warehouse not found!",
             });
   
             return;
           }
 
           req.warehouse = warehouse;
           next();
         })
         .catch((err) => {
           res.status(404).json({
             status: "FAIL",
             message: "Warehouse not found!",
           });
         });
     },
   
 };
 
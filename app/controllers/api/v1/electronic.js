/**
 * @file contains request handler of post resource
 * @author Safira Tyas Wandita
 */

const { Electronics, Warehouse } = require("../../../models");
const electronicService = require("../../../services/electronic");

module.exports = {
  list(req, res) {
    electronicService
      .getAll({
        include: {
          model: Warehouse
        },
      })
      .then((electronics) => {
        res.status(200).json({
          status: "OK",
          data: {
            electronics,
          },
        });
      })
      .catch((err) => {
        res.status(400).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  create(req, res) {
    const {
      name,
      color,
      brand,
      price,
      warehouseId
    } = req.body;
    electronicService
      .create(name, color, brand, price, warehouseId)
      .then((electronic) => {
        res.status(201).json({
          status: "OK",
          data: electronic,
        });
      })
      .catch((err) => {
        res.status(201).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  update(req, res) {
    electronicService
      .update(req.electronic, req.body)
      .then((electronic) => {
        res.status(200).json({
          status: "OK",
          data: electronic,
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  show(req, res) {
    const electronic = req.electronic;

    res.status(200).json({
      status: "OK",
      data: electronic,
    });
  },

  destroy(req, res) {
    electronicService
      .delete(req.electronic)
      .then(() => {
        res.status(204).end();
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  setElectronic(req, res, next) {
    electronicService
      .choose(req.params.id)
      .then((electronic) => {
        if (!electronic) {
          res.status(404).json({
            status: "FAIL",
            message: "Electronic not found!",
          });

          return;
        }

        req.electronic = electronic;
        next();
      })
      .catch((err) => {
        res.status(404).json({
          status: "FAIL",
          message: "Electronic not found!",
        });
      });
  },

};
/**
 * @file contains entry point of controllers api v1 module
 * @author Fikri Rahmat Nurhidayat
 */

const electronic = require("./electronic");
const warehouse = require("./warehouse");

module.exports = {
  electronic,
  warehouse
};

const {
    Warehouse
} = require("../models")

module.exports = {
    getAll() {
        return Warehouse.findAll();
    },
    create(name, address, owner) {
        return Warehouse.create({
            name,
            address,
            owner
        });
    },
    update(oldWarehouse, requestBody) {
        return oldWarehouse.update(requestBody);
    },

    choose(id) {
        return Warehouse.findByPk(id);
    },
    delete(oldWarehouse) {
        return oldWarehouse.destroy();
    },

}
const {
    Electronics
} = require("../models")

module.exports = {
    getAll() {
        return Electronics.findAll();
    },
    create(name, color, brand, price, warehouseId) {
        return Electronics.create({
            name,
            color,
            brand,
            price,
            warehouseId
        });
    },
    update(oldElectronic, requestBody) {
        return oldElectronic.update(requestBody);
    },

    choose(id) {
        return Electronics.findByPk(id);
    },
    delete(oldElectronic) {
        return oldElectronic.destroy();
    },

}